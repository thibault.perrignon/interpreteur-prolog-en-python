# PCOMP - Projet : Interprète Prolog

Le projet est un interprète Prolog, à réaliser au choix en OCaml, Java ou Python.

L'énoncé est disponible sur Moodle.

Vous devez faire un _fork_ privé par binôme de ce squelette de projet, et y ajouter votre binôme ainsi que les enseignants du cours (Carlos Agon, Emmanuel Chailloux, Basile Pesin, Antoine Miné, Christine Tasson) comme _maintainer_.
Un seul membre du binôme fait le _fork_, mais les deux membres du binôme peuvent faire un _clone_ du _fork_ du projet afin de travailler ensemble.
Ajoutez les fichiers de votre projet au fur et à mesure (`git add`, `git commit`, `git push`).
N'oubliez pas de remplir le fichier `RENDU.md` pour chaque jalon.
Il est conseillé de faire des commit réguliers.

Les répertoires `java`, `ocaml`, `python` de ce squelette contiennent une structure d'AST et un analyseur syntaxique dans chacun des langages proposés.
Le répertoire `exemples`, que vous pouvez enrichir, contient quelques exemples de programmes Prolog.
Référez-vous au fichier `README.md` de chaque répertoire pour plus d'informations sur la structure des sources et sur la procédure de compilation et de test de l'analyseur syntaxique.


# Commentaires Christine Tasson -

## Rapport:

Très clair. 
- Attention à la grammaire et à l'orthographe.
- Vous devez ajouter un descriptif des algorithmes implémentés pour tous les jalons. D'autant plus que le code des algorithmes n'est pas commenté.
- Il manque de détailler les choix que vous avez faits dont vous êtes particulièrement fiers et les difficultés que vous avez rencontrées et quelles solutions vous avez proposées.



## Jalon 1:

J'aimerais implémenter l'unification des équations de l'exercice 7 du TD4. Je n'ai pas trouvé de documentation pour le faire facilement.
Pouvez vous y remédier soit en ajoutant la documentation, soit en ajoutant les tests.

Commenter le code d'algorithme.py pour expliquer les algorithmes de substitution, d'occurcheck et d'unification

## Jalon 2:

Commenter le code d'interprete2.py pour expliquer l'algorithme

## Jalon 3:

Dans le fichier solve.py, ça ne me paraît une mauvaise idée de modifier la liste sur laquelle vous itérez.
Commentez vos algorithmes pour les rendre compréhensibles au lecteur.

## Jalon 4:

Commentez les structures (journal.py), le backtracking et les algorithmes (solve)
Ajoutez plus de tests.


## Jalon 5:

Pour lancer `jalon5.py`, j'ai dû renommer les chemins des fichiers exemples (après ça marche, mais si vous pouviez l'améliorer ce serait mieux): 
`FileNotFoundError: [Errno 2] No such file or directory: '/home/tasson/Enseignement/SU/L3-pCOMP/projet/2022/projet-etu/Thibault.Perrignon-Sommet-ben.abdillahi/projet-pcomp-2022/python./../exemples/classification.pl'`

Le toplevel sur l'exemple `classification.pl` ne fonctionne pas. `?- trace.` engendre une erreur, et une requête donne la réponse et sort du porgramme sans redonner la main à l'utilisateur.

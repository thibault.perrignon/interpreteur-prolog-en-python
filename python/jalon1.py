from prolog_ast.ast import *
from jalon1.testEquation import testEquation
from jalon1.testOccurCheck import testOccurCheck
from jalon1.testEnvironnement import testEnvironnement
from jalon1.testAlgos import testSubst
from jalon1.testAlgos import testUnify
from jalon1.algos import ErreurUnification


print('\nTest equation :')
testEquation()

print('\nTest occurCheck :')
testOccurCheck()

print('\nTest environnement :')
testEnvironnement()

print('\nTest substitution :')
testSubst()

print('\nTest unification :')
for i in range(1,5):
    try:
        testUnify(i)
    except ErreurUnification as e:
        print(e)


from prolog_ast.ast import *
from jalon4.testJournal import *
from jalon4.testSolve import *
from jalon4.testInterprete4 import *

print('\nTest Journal : ')
testJournal()

print('\nTest Solve : ')
testSolve('/jalon4/programme0.pl')
testSolve('/jalon4/classificationM.pl')

print('\nTest Interprete4 : ')
testInterprete4('/jalon4/classificationM.pl')
testInterprete4('/jalon4/genealogie.pl')
testInterprete4('/jalon4/listes.pl')

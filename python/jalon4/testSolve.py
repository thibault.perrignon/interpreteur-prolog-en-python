from prolog_ast.ast import *
from prolog_parser import parser
from jalon4.solve import *
from jalon4.journal import Journal
from jalon1.environnement import Environnement
from sys import path

p=path[0]

def testSolve(prog):
    print(f'\nprogramme : {prog}')
    a=parser.parseFile(f'{p}{prog}')

    rules=[]
    goals=[]

    for d in a.decls:
        match d:
            case DeclAssertion():
                rules+=[d]
            case DeclGoal():
                goals=d.preds
    try:
        print(f"init :\n- goals : {goals}\n- rules : {rules}")
        env2=solve(Journal(),goals,rules,Environnement())
        print(f"fonction : solve(Journal(),goals,rules,Environnement())\nfinal :\n- env : {env2}")
    except:
        print('erreur')
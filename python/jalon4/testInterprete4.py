from prolog_ast.ast import *
from prolog_parser import parser
from jalon3.choose import *
from jalon1.environnement import Environnement
from sys import path

from jalon4.interprete4 import interprete4

p=path[0]

def testInterprete4(prog):
    print(f'\nprogramme : {prog}')
    a=parser.parseFile(f'{p}{prog}')

    print(a)
    try:
        print('solution : ',end='')
        interprete4(a)
    except ErreurUnification:
        print("Il n'existe pas de solution")
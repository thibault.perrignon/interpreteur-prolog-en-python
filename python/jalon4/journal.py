from prolog_ast.ast import *
from jalon1.algos import *

class Journal:
    def __init__(self):
        self.choix=[]
    def addChoix(self,goals:list[Predicate],rules:list[DeclAssertion],env:Environnement):
        self.choix+=[(goals,rules.copy(),env)]
    def getChoix(self):
        match self.choix:
            case []:
                return None 
            case [*a,(_,[],_)]:
                self.choix=a
                return self.getChoix()
            case [*a,b]:
                r = b[1].pop(0)
                if len(b[1])==0:
                    self.choix=a
                return (b[0],r,b[2])
    def isEmpty(self):
        return len(self.choix)==0
    def __repr__(self):
        r = 'Journal :\n'
        if len(self.choix)==0:
            return r+'  -Vide'
        for i in range(len(self.choix)):
            r+= f'  -Choix {i+1}:\n'
            r+= f'      -Goals : {self.choix[i][0]}\n'
            r+= f'      -Rules : {self.choix[i][1]}\n'
            r+= f'      -Env : {self.choix[i][2]}\n'
        return r
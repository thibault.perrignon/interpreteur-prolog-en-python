from prolog_ast.ast import *
from jalon1.algos import *
from jalon1.equation import *
from jalon4.solve import *
from jalon4.journal import *

def interprete4(prog:Program):
    rules=[]

    for d in prog.decls:
        match d:
            case DeclAssertion():
                rules+=[d]
            case DeclGoal():
                goals=d.preds
    try:
        env=solve(Journal(),goals,rules,Environnement())
    except:
        print('Faux')
        return

    for g in goals:
        tsol=set(t for t in env.environnement if OccurCheck(TermVariable(t),TermPredicate(g)))
    
    nenv=Environnement()
    for t in tsol:
        nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))
        while nenv.environnement[t]!=env.environnement[t]:
            env.addSubstitution(TermVariable(t),nenv.environnement[t])
            nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))
            
    sol={t:env.environnement[t] for t in tsol if OccurCheck(TermVariable(t),TermPredicate(g))}
    if len(sol):
        print(Environnement(sol))
    else:
        print('Vrai')

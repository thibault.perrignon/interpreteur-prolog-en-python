from jalon1.algos import *
from jalon3.choose import *
from jalon4.journal import Journal

def solve(j:Journal,goals:list[Predicate],rules:list[DeclAssertion],env:Environnement,trace:bool=False,i:int=[1]):
    rd = True #Trace
    if j.choix is None:
        raise SolNotFound

    if j.isEmpty():
        rd = False #Trace
        j.addChoix(goals.copy(),rules,env)

    nbGoal = -1 #Trace

    

    while not j.isEmpty():
        g,r,e=j.getChoix()
        
        try:
            env,ngoals=choose(i[0],e,g[0],[r])
            
            if trace: #Trace
                if nbGoal == -1:
                    if len(j.choix) != 0:
                        nbGoal = len(j.choix[-1][0])
                    else:
                        nbGoal = 0

                if rd:
                    nbGoal-=1
                    print(f"REDO : {g[0]} {g[0].pos}")
                    rd = False
                elif nbGoal>0 and len(j.choix[-1][0])<=nbGoal:
                    nbGoal-=1
                    print(f"GOAL : {g[0]} {g[0].pos}")
                print(f"CALL : {r} {r.pos}")

            i[0]+=1
            gf=ngoals+g[1:]
            if len(gf)==0:
                if j.isEmpty():
                    j.choix=None
                return env
            j.addChoix(gf,rules,env)
        except ErreurUnification as e:
            pass
    raise SolNotFound

class SolNotFound(Exception):
    pass
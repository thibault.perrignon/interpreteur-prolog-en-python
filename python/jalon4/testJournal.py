from prolog_ast.ast import *
from jalon3.solve import *
from sys import path

from jalon4.journal import Journal

p=path[0]

def testJournal():
    j=Journal()
    print(f'Creation du journal initialement vide :\n{j}')

    j.addChoix('g',['r1','r2','r3'],'e')
    j.addChoix('g2',['r5','r8'],'e7')
    print(f'\nAjout de choix :\n{j}')

    print(f"Recuperation d'un choix possible : {j.getChoix()}")
    print(j)
    print(f"Recuperation d'un choix possible : {j.getChoix()}")
    print(j)

    j.addChoix('g3',['r21','r22'],'e10')
    print(f'\nAjout de choix :\n{j}')

    print(f"Recuperation d'un choix possible : {j.getChoix()}")
    print(j)
    print(f"Recuperation d'un choix possible : {j.getChoix()}")
    print(j)

    print(f"Elimination des choix sans régle avant la recuperation d'un choix possible : {j.getChoix()}")
    print(j)
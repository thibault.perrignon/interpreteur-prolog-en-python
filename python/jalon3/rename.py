
from prolog_ast.ast import *

def rename(n:int,a:DeclAssertion|TermPredicate|TermVariable):

    match a:
        case TermVariable():
            a2=TermVariable(a.name+str(n))
        case TermPredicate():
            a2=TermPredicate(Predicate(a.pred.symbol,[rename(n,t) for t in a.pred.args]))
        case DeclAssertion():
            ph=Predicate(a.head.symbol,[rename(n,t) for t in a.head.args])
            pp=[Predicate(p.symbol,[rename(n,t) for t in p.args]) for p in a.preds]
            a2=DeclAssertion(ph,pp)
    return a2
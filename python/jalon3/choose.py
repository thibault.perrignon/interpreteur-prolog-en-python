from prolog_ast.ast import *
from jalon1.algos import *
from jalon1.equation import *
from jalon3.rename import *

def choose(n:int,env:Environnement,goal:Predicate,rules:list[DeclAssertion]):
    for r in rules:
        r=rename(n,r)
        sEqu=SysEquation([Equation(TermPredicate(goal),TermPredicate(r.head))])
        try:
            unify(env,sEqu)
        except ErreurUnification:
            continue
        sEqu.popEquation()
        sEqu.addEquation(Equation(TermPredicate(goal),TermPredicate(r.head)))
        
        env2=unify(env,sEqu)
        
        return env2,r.preds
    raise ErreurUnification(goal,rules)
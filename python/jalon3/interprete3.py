from prolog_ast.ast import *
from jalon1.algos import *
from jalon1.equation import *
from jalon3.choose import *
from jalon3.solve import *

def interprete3(prog:Program):
    rules=[]

    for d in prog.decls:
        match d:
            case DeclAssertion():
                rules+=[d]
            case DeclGoal():
                goals=d.preds

    env=solve(goals,rules)

    sol={}
    for g in goals:
        sol.update({x:subst(env.environnement[x],env) for x in env.environnement if OccurCheck(TermVariable(x),TermPredicate(g))})
    return Environnement(sol)
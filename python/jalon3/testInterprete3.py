from prolog_ast.ast import *
from prolog_parser import parser
from jalon3.choose import *
from jalon1.environnement import Environnement
from sys import path

from jalon3.interprete3 import interprete3

p=path[0]

def testInterprete3(prog):
    print(f'\nprogramme : {prog}')
    a=parser.parseFile(f'{p}{prog}')

    print(a)
    try:
        print(f'solution : {interprete3(a)}')
    except ErreurUnification:
        print("Il n'existe pas de solution")
from prolog_ast.ast import *
from prolog_parser import parser
from jalon3.choose import *
from jalon1.environnement import Environnement
from sys import path

p=path[0]

def testChoose(prog):
    print(f'\nprogramme : {prog}')
    a=parser.parseFile(f'{p}{prog}')

    rules=[]
    
    for d in a.decls:
        match d:
            case DeclAssertion():
                rules+=[d]
            case DeclGoal():
                goal=d.preds[0]

    try:
        env2,body=choose(1,Environnement(),goal,rules)
        print(f"init :\n- goal : {goal}\n- rules : {rules}\nfonction : choose(1,Environnement(),goal,rules)\nfinal :\n- body' : {body}\n- env : {env2}")
    except ErreurUnification as e:
        print(e)
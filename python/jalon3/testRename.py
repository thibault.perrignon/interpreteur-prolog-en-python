from prolog_ast.ast import *
from jalon3.rename import *

def testRename():
    X=TermVariable('X')
    Z=TermVariable('Z')
    W=TermVariable('W')
    XX=TermVariable('XX')

    ph=Predicate('p',[Z,TermPredicate(Predicate('h',[Z,W])),TermPredicate(Predicate('f',[W]))])
    pp=[Predicate('r',[X,XX]),Predicate('q',[XX])]

    a=DeclAssertion(ph,pp)

    a1=rename(1,a)

    print(f"Assertion initial : {a}\nrename(1,Assertion) : {a1}")
from prolog_ast.ast import *
from jalon1.algos import *
from jalon1.equation import *
from jalon3.choose import *

def solve(goals:list[Predicate],rules:list[DeclAssertion]):
    env=Environnement()
    goalsc=goals.copy()

    for n,g in enumerate(goalsc):
        env,agoals=choose(n,env,g,rules)
        goalsc+=agoals

    return env
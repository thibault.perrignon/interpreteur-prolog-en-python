from prolog_ast.ast import *
from jalon3.testRename import testRename
from jalon3.testChoose import testChoose
from jalon3.testSolve import testSolve
from jalon3.testInterprete3 import testInterprete3

print('\nTest Rename : ')
testRename()

print('\nTest Choose : ')
testChoose('/jalon3/programme0.pl')
testChoose('/jalon3/programme1.pl')
testChoose('/jalon3/programme2.pl')

print('\nTest Solve : ')
testSolve('/jalon3/programme0.pl')
testSolve('/jalon3/programme3.pl')

print('\nTest interprete3 : ')
testInterprete3('/jalon3/programme3.pl')
testInterprete3('/jalon3/programme4.pl')
testInterprete3('/jalon3/programme5.pl')


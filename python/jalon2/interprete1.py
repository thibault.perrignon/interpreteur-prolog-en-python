from jalon1.equation import *
from jalon1.algos import unify
from jalon1.environnement import Environnement
from prolog_ast.ast import *

def interprete1(a:Program):
    dasser=[]
    dgoal=[]
    for d in a.decls:
        match d:
            case DeclAssertion():
                dasser += [TermPredicate(d.head)]
            case DeclGoal():
                dgoal += [TermPredicate(d.preds[0])]

    for i in dasser:
        if i.pred.symbol == dgoal[0].pred.symbol:
            asser = i
            break
    eq = Equation(asser,dgoal[0])
    sEq = SysEquation([eq])
    
    env = unify(Environnement(),sEq)
    return env
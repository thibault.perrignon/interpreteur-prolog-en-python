from jalon2.interprete0 import interprete0
from prolog_ast.ast import *
from prolog_parser import parser
from sys import path

p=path[0]

def testInterprete0():
    a=parser.parseFile(f'{p}/jalon2/programme0.pl')
    b=parser.parseFile(f'{p}/jalon2/programme1.pl')
    print(f'Programme 0 :\n{a}')
    try:
        env=interprete0(a)
        print(f'Environnement :\n{env}\n')
    except Exception as e:
        print(e)

    print(f'Programme 1 :\n{b}')
    try:
        env=interprete0(b)
        print(f'Environnement :\n{env}\n')
    except Exception as e:
        print(e)
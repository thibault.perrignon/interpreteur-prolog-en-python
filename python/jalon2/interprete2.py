from jalon1.equation import *
from jalon1.algos import unify
from jalon1.environnement import Environnement
from prolog_ast.ast import *

def interprete2(a:Program):
    dasser=[]
    dgoal=[]
    eq=[]


    for d in a.decls:
        match d:
            case DeclAssertion():
                dasser += [TermPredicate(d.head)]
            case DeclGoal():
                for i in d.preds:
                    dgoal += [TermPredicate(i)]

    for i in dgoal:
        for j in dasser:
            if i.pred.symbol == j.pred.symbol:
                eq += [Equation(i,j)]
                break

    sEq = SysEquation(eq)
    
    env = unify(Environnement(),sEq)
    return env
from jalon2.interprete1 import interprete1
from prolog_ast.ast import *
from prolog_parser import parser
from sys import path

p=path[0]

def testInterprete1():
    a=parser.parseFile(f'{p}/jalon2/programme0.pl')
    b=parser.parseFile(f'{p}/jalon2/programme2.pl')
    c=parser.parseFile(f'{p}/jalon2/programme3.pl')

    print(f'Programme 0 :\n{a}')
    try:
        env=interprete1(a)
        print(f'Environnement :\n{env}\n')
    except Exception as e:
        print(e)

    print(f'Programme 2 :\n{b}')
    try:
        env=interprete1(b)
        print(f'Environnement :\n{env}\n')
    except Exception as e:
        print(e)

    print(f'Programme 3 :\n{c}')
    try:
        env=interprete1(c)
        print(f'Environnement :\n{env}\n')
    except Exception as e:
        print(e)
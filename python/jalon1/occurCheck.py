from prolog_ast.ast import *

# Lève une exception en cas d'erreur, retourne True si var est present dans term, 
# False sinon
def OccurCheck(var: TermVariable, term: Term):
    match (var,term):
        case (TermVariable(),TermVariable()):
            return var == term
        case (TermVariable(),TermPredicate()):
            return any([OccurCheck(var,t) for t in term.pred.args])
        case _:
            raise TypeError

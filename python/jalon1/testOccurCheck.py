from ast import Assert
from jalon1.occurCheck import *

def testOccurCheck():
    t1 = TermVariable('a')
    t2 = TermVariable('b')
    t3 = TermVariable('c')
    p1 = Predicate('p1', [t1, t2])
    t4 = TermPredicate(p1)
    p2 = Predicate('p2', [t1, t2, t4])
    t5 = TermPredicate(p2)
    p3 = Predicate('p3', [t5, t4, t3])
    t6 = TermPredicate(p3)
    

    print(f'test OccurCheck({t1}, {t1}) :\n   Resultat : {OccurCheck(t1, t1)}')
    
    print(f'test OccurCheck({t4}, {t1}) :\n   Resultat : ',end='')
    try:
        print(OccurCheck(t4, t1))
    except TypeError:
        print('TypeError')

    print(f'test OccurCheck({p1}, {t1}) :\n   Resultat : ',end='')
    try:
        print(OccurCheck(p1, t1))
    except TypeError:
        print('TypeError')
    
    print(f'test OccurCheck({t1}, {t2}) :\n   Resultat : {OccurCheck(t1, t2)}')
    
    print(f'test OccurCheck({t2}, {t4}) :\n   Resultat : {OccurCheck(t2, t4)}')
    
    print(f'test OccurCheck({t3}, {t4}) :\n   Resultat : {OccurCheck(t3, t4)}')

    print(f'test OccurCheck({t3}, {t5}) :\n   Resultat : {OccurCheck(t3, t5)}')
    
    print(f'test OccurCheck({t3}, {t6}) :\n   Resultat : {OccurCheck(t3, t6)}')
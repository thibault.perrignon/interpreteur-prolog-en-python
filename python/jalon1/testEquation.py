from jalon1.equation import *


def testEquation():
    t1 = TermVariable('a')
    t2 = TermVariable('b')
    t3 = TermVariable('c')
    p1 = Predicate('p1', [t1, t2])
    t4 = TermPredicate(p1)
    e1 = Equation(t1, t2)
    e2 = Equation(t3, t4)
    e3 = Equation(t2, t4)
    e4 = Equation(t1, t3)

    listEquations = SysEquation()
    listEquations.addEquation(e1)
    listEquations.addEquation(e2, e3, e4)
    print(f"Systeme d'equations : {listEquations}")
    
    e5 = listEquations.popEquation()
    print(f"Test popEquation :\n-Equation retirée : {e5}\n-Systeme d'equation : {listEquations}")
    print("Test leftEquation : ", listEquations.leftEquation())
    print("Elimination des derniers elements :")
    for i in range(len(listEquations.equations)):
        print("pop: ", listEquations.popEquation())

    print("Test leftEquation : ", listEquations.leftEquation())
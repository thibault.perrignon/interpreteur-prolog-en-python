from prolog_ast.ast import *
from jalon1.environnement import Environnement
from jalon1.occurCheck import OccurCheck
from jalon1.equation import SysEquation
from jalon1.equation import Equation
import copy

def subst(t: Term, env: Environnement):
    # var est un string, on recupere une variable dans var_tmp
    for var in env.environnement:
        var_tmp = TermVariable(var)
        if OccurCheck(var_tmp, t):
            # var est dans t
            match t:
                case TermVariable():
                    return env.environnement[var]
                case _:
                    t:TermPredicate
                    for i in range(len(t.pred.args)):
                        if OccurCheck(var_tmp, t.pred.args[i]) == 1:
                            env_tmp = {var:env.environnement[var]}
                            env_tmp = Environnement(env_tmp)
                            t.pred.args[i] = subst(t.pred.args[i], env_tmp)
    return t

def unify(env: Environnement, equa: SysEquation):
    env2=copy.deepcopy(env)
    equa2=copy.deepcopy(equa)

    while equa2.leftEquation():
        e1 = equa2.popEquation()
        
        t1 = subst(e1.term1, env2)
        t2 = subst(e1.term2, env2)

        match t1,t2:
            case (TermVariable(),TermVariable()):
                if OccurCheck(t1, t2) == 0:
                    env2.addSubstitution(t1, t2)
            case (TermVariable(),TermPredicate()):
                if OccurCheck(t1, t2) == 0:
                    env2.addSubstitution(t1, t2)
            case (TermPredicate(),TermVariable()):
                if OccurCheck(t2, t1) == 0:
                    env2.addSubstitution(t2, t1)
            case (TermPredicate(),TermPredicate()):
                if t1.pred.symbol == t2.pred.symbol and len(t1.pred.args)==len(t2.pred.args):
                    sEqu=SysEquation()
                    for i,j in zip(t1.pred.args,t2.pred.args):
                        sEqu.addEquation(Equation(i,j))
                    #print(sEqu)
                    env2=unify(env2,sEqu)
                else:
                    raise ErreurUnification(e1,env2)
    return env2

class ErreurUnification(Exception):
    def __init__(self,*args):
        match args:
            case [equ,Environnement()]:
                self.equ = equ
                self.env = args[1]
                super().__init__(f"Erreur d'unification : \n- equation : {equ},\n- environnement : {self.env}")
            case [goals,rules]:
                self.goals = goals
                self.rules = rules
                super().__init__(f"Erreur d'unification : \n- unification impossible entre : \n\t{goals} \net \n\t{rules}")

from jalon1.environnement import *
from jalon1.occurCheck import OccurCheck

def testEnvironnement():
    env = Environnement()
    print(f'Creation environnement : {env}')
    t1 = TermVariable('a')
    t2 = TermVariable('b')
    t3 = TermVariable('c')
    p1 = Predicate('p1', [t1, t2])
    t4 = TermPredicate(p1)
    p2 = Predicate('p2', [t1, t2, t4])
    t5 = TermPredicate(p2)
    p3 = Predicate('p3', [t5, t4, t3])
    t6 = TermPredicate(p3)

    vars = [t1, t2, t3]
    term = [t4, t5, t6]

    for v in vars:
        for t in term:
            if not OccurCheck(v, t):
                print(f'Ajout de substitution : {v} -> {t}')
                env.addSubstitution(v, t)
    print(f'Environnement apres ajout des substitutions : {env}')

    #test effectue avec un TermPredicate en premiere argument pour tester l'erreur
    print('Test erreur de substitution : ')
    print(f'Ajout de {t4} -> {t1}\n- ',end='')
    env.addSubstitution(t4, t1)
    print(f'Ajout de lambda x:x -> "a"\n- ',end='')
    env.addSubstitution(lambda x:x, 'a')

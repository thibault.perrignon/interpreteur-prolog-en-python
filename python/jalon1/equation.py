from prolog_ast.ast import *

class Equation:

    term1: Term
    term2: Term

    def __init__(self, term1: Term, term2: Term):
        self.term1 = term1
        self.term2 = term2

    # Conversion en chaine
    def __str__(self) -> str:
        return str(self.term1) + '=' + str(self.term2)

    def __repr__(self) -> str:
        return self.__str__()

class SysEquation:
    equations: list[Equation]

    def __init__(self, equations: list[Equation]=None):
        if equations is None:
            equations = []
        self.equations = equations

    # Conversion en chaine
    def __str__(self) -> str:
        return '{' + ', '.join(map(str, self.equations)) + '}'
    
    def __repr__(self) -> str:
        return self.__str__()
    
    # Ajout d'equations 
    def addEquation(self, *varEq: list[Equation]):
        self.equations = [*self.equations, *varEq]
    
    # Retire la i-eme equation ou la premiere par defaut
    def popEquation(self,i=0):
        return self.equations.pop(i)
    
    # Retourne True s'il reste des equations, False sinon
    def leftEquation(self):
        return len(self.equations) != 0

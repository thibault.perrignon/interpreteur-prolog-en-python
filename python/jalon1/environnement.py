from prolog_ast.ast import *

class Environnement:
    environnement: dict[TermVariable:Term]

    def __init__(self, environnement: dict[TermVariable:Term]=None):
        if environnement is None:
            environnement = {}
        self.environnement = environnement
    
    # Conversion en chaine
    def __str__(self) -> str:
        tmp = '{'
        for key in self.environnement:
            tmp = tmp + str(key) +" -> "+str(self.environnement[key])+", "
        if len(tmp) < 4:
            return tmp + "}"
        return tmp[:-2] + "}"
    
    def __repr__(self) -> str:
        return self.__str__()
    
    # Ajoute la substitution var -> term a l'environnement
    # Attention, var etant <unhashable>, son nom sert de cle'
    def addSubstitution(self, var: TermVariable, term: Term):
        match (var,term):
            case (TermVariable(),Term()):
                self.environnement[var.name] = term
            case (_,Term()):
                print("tried adding a non var to the environnement when a var was expected on the left:", var)
            case (TermVariable(),_):
                print("tried adding a substitution of a var with a non-term to the environement when a term was expected on the right:", term)
            case _:
                print(f'wrong substitution: {type(var).__name__} -> {type(term).__name__}')
    

    

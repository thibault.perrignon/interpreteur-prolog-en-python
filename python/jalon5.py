from prolog_ast.ast import *
from jalon5.toplevel import toplevel
from jalon5.interprete5 import interprete5

print('\nTest Toplevel : ')
toplevel('/../exemples/classification.pl')

print('\nTest Interprete5 :\nc "exit" pour sortir du programme \n "trace" pour activer/désactiver le trace\nProgramme : ')
interprete5('./../exemples/classification.pl')
#interprete5('./../exemples/genealogie.pl')
#interprete5('/../exemples/listes.pl')

from prolog_ast.ast import *
from prolog_parser import parser
from prolog_parser.PrologParser import NoViableAltException
from jalon1.algos import *
from jalon1.equation import *
from jalon4.solve import *
from jalon4.journal import *
from sys import path

p=path[0]

def interprete5(progPath:str):
    prog = parser.parseFile(f'{p}{progPath}')
    print(prog)

    rules = prog.decls

    trace = False

    while True:
        req = '?-' + input("requête : ?- ")
        if req[2:].strip() in ('','.'):
            continue
        if 'trace' in req:
            trace = not trace
            print(f"trace = {trace}")
            continue
        if 'exit' in req:
            break

        goals = parser.parseString(req).decls[0].preds

        try:
            j = Journal()
            cont = True
            while cont:
                env=solve(j,goals,rules,Environnement(),trace)

                ng = TermPredicate(Predicate('',[TermPredicate(x) for x in goals]))

                nenv = Environnement(env.environnement.copy())
                tenv = Environnement(env.environnement.copy())
                for i,x in enumerate(env.environnement.items()):

                    if isinstance(x[1],TermVariable):
                        nenv = Environnement()
                        
                        for e in tenv.environnement.items():
                            if e == x:
                                continue
                            
                            if OccurCheck(TermVariable(x[0]),ng):
                                if e[0]==x[1].name and not OccurCheck(x[1],ng):
                                    
                                    nenv.environnement[x[0]]=e[1]
                                elif OccurCheck(x[1],e[1]) and not OccurCheck(x[1],ng):
                                    
                                    nenv.environnement[e[0]]=subst(e[1],Environnement({x[1].name:TermVariable(x[0])}))
                                else:
                                    nenv.environnement[e[0]]=e[1]
                            else:
                                if e[0]==x[0]:
                                    
                                    nenv.environnement[x[1].name]=e[1]
                                elif OccurCheck(TermVariable(x[0]),e[1]):
                                    
                                    nenv.environnement[e[0]]=subst(e[1],Environnement({x[0]:x[1]}))
                                else:
                                    nenv.environnement[e[0]]=e[1]
                    
                    tenv.environnement=nenv.environnement.copy()


                env = Environnement({x[0]:x[1] for x in nenv.environnement.items() if isinstance(x[1],TermPredicate) or (OccurCheck(TermVariable(x[0]),ng) and OccurCheck(x[1],ng))})

                other = Environnement({x[0]:x[1] for x in nenv.environnement.items() if isinstance(x[1],TermVariable) and (not OccurCheck(TermVariable(x[0]),ng) or not OccurCheck(x[1],ng))})

                for o in other.environnement.items():
                    for n in nenv.environnement.items():
                        env.addSubstitution(TermVariable(n[0]),subst(n[1],Environnement({o[1].name:TermVariable(o[0])})))

                env = Environnement({x[0]:x[1] for x in env.environnement.items() if isinstance(x[1],TermPredicate) or (OccurCheck(TermVariable(x[0]),ng) and OccurCheck(x[1],ng))})

                nenv=Environnement()
                for t in env.environnement:
                    nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))
                    while nenv.environnement[t]!=env.environnement[t]:
                        env.addSubstitution(TermVariable(t),nenv.environnement[t])
                        nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))

                nenv = Environnement({x[0]:x[1] for x in nenv.environnement.items() if OccurCheck(TermVariable(x[0]),ng) and (not isinstance(x[1],TermVariable) or x[0]!=x[1].name)})

                if len(nenv.environnement):
                    print('=> ',nenv)
                    if input('Next ?(o/n)')=='n':
                        cont = False
                else:
                    print('=> Vrai')
                    cont = False
        except SolNotFound:
            print('=> Faux')

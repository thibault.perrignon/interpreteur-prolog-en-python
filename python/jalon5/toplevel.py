from prolog_ast.ast import *
from prolog_parser import parser
from jalon1.algos import *
from jalon1.equation import *
from jalon4.solve import *
from jalon4.journal import *
from sys import path

p=path[0]

def toplevel(progPath:str):
    prog = parser.parseFile(f'{p}{progPath}')
    print(prog)

    rules = prog.decls

    req = '?-' + input("requête : ?- ")
    goals = parser.parseString(req).decls[0].preds

    try:
        env=solve(Journal(),goals,rules,Environnement())
        for g in goals:
            tsol=set(t for t in env.environnement if OccurCheck(TermVariable(t),TermPredicate(g)))
    
        nenv=Environnement()
        for t in tsol:
            nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))
            while nenv.environnement[t]!=env.environnement[t]:
                env.addSubstitution(TermVariable(t),nenv.environnement[t])
                nenv.addSubstitution(TermVariable(t),subst(env.environnement[t],env))
            
        sol={t:env.environnement[t] for t in tsol if OccurCheck(TermVariable(t),TermPredicate(g))}
        if len(sol):
            print(Environnement(sol))
        else:
            print('Vrai')
    except:
        print('Faux')

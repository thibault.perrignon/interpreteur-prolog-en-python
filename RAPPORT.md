3812573 : Abdillahi Ben Idris  
21104636 : Thibault Perrignon-Sommet

Projet ecrit en [**Python3.10**](https://docs.python.org/3/whatsnew/3.10.html) pour tirer partie du partern matching.

# Jalon 1 - 28/02/22_10:00

## equation.py
Le fichier contient deux classes, `Equation` et `SysEquation`. La première contient deux attributs `Term` pour représenter les termes et la seconde un attribut `list` pour stocker les équations. La liste est simple à manipuler pour l'ajout et le retrait d'équations

## occurCheck.py
Contient la fonction `OccurCheck`. Lève une exception si le premier argument n'est pas un `TermVariable`, retourne **True** si la variable est présente dans le terme en second argument et **False** sinon

## environnement.py
Contient une classe `Environnement` ayant un attribut de type dictionnaire. Elle requiert des `TermVariable` en clés, qui sont ensuite associées aux `Term`. La structure ne permet pas d'utiliser les `TermVariable` en tant que clé donc on utilise le nom de celle-ci. Elle doit ensuite être reconvertie si besoin dans les autres fonctions.

## algos.py
Contient les fonctions `subst` et `unify`. `subst` parcourt les variables de l'`Environnement env` et retourne le `Term t` une fois les substitutions effectuées. Les substitutions sont effectuées une variable à la fois. `unify` parcourt les équations du `SysEquation equa` et ajoute une substitution à l'`Environnement env` si l'un des deux termes de l'équation est un `TermVariable`, sinon on décompose le système et on utilise `unify` sur ce nouveau système avec le même `Environnement` puis on retourne l'`Environnement env`.

## structure de donnée
* **Pour les équations** : une équation est une `class` avec un attribut pour chaque terme et un système d'équation est représenté par une `class` avec pour attribut une liste d'équation.
* **Pour l'environnement** : Les substitutions de l'environnement sont stockées dans un dictionnaire dont la clé est le nom de la `var`. 

## jalon1.py et tests
Chaque fichier cité précédemment possède son fichier test. On y trouve des fonctions sans argument qui créent les variables voulues et réalisent des affichages pour s'assurer du bon fonctionnement des fonctions. On peut choisir les fonctions en les décommentant dans *jalon1.py*.Elles y sont ensuite exécutées. 

## lancer jalon1.py
Tous les fichiers, à part *jalon1.py*, se trouvent dans le répertoire **jalon1**. Pour exécuter *jalon1.py*, il faut à priori se placer dans le répertoire *projet-pcomp-2022/python* pour pouvoir lancer *./jalon1* avec python 3.10.


# Jalon 2 - 07/03/22_10:00

## interprete
Les fichiers interprete0.py, interprete1.py et interprete2.py contiennent les algorithmes pour interpréter des programmes. 

## programme
* Le programme0.pl contient un programme d'une `assertion` et d'un seul `goal`.
* Le programme1.pl contient un programme d'une `assertion` et d'un seul `goal` et n'est pas unifiable.
* Le programme2.pl et le programme3.pl contiennent chacun un programme de 3 `assertion` et d'un seul `goal`.
* Le programme4.pl contient un programme de 3 `assertion` et de plusieurs `goal`.

## tests
Chaque interpréte possède un fichier de test qui affiche le `programme prolog` testé puis affiche l'`environnement` obtenu à l'aide de l'interpréte. S'il y a une erreur d'unification, le programme affiche l'`equation` qui pose problème et l'`environnement`.

## lancer jalon2.py
Tous les fichiers, à part *jalon2.py*, se trouvent dans le répertoire **jalon2**. Pour exécuter *jalon2.py*, il faut avoir python 3.10.

# Jalon 3 - 14/03/22_10:00

## rename.py
Contient la fonction `rename` qui prend en entrée un `int n` et une `DeclAssertion|TermPredicate|TermVariable a`.
Si on a en entrée un `TermVariable`, on retourne un nouveau `TermVariable` renommé. 
Si on a un `TermPredicate`, on construit un nouveau `TermPredicate` où on applique la fonction `rename` à chaque `args`.
Si on a une `DeclAssertion` alors on construit une nouvelle `DeclAssertion` où on applique la fonction `rename` à chaque `args` de `head` et à chaque `args` de chaque `Predicate` de `preds`.

## choose.py
Contient la fonction `choose` qui prend en entrée un `int n`, un `Environnement env`, un `Predicate goal` et une liste de `DeclAssertion rules`.
La fonction essaye d'unifier le `goal` avec les `rules` si elle réussi, elle renvoie le nouvel `Environnement` et le `body` de la `DeclAssertion` avec qui le `goal` a été unifié.
Sinon, elle lève une exception.

## solve.py
Contient la fonction `solve` qui prend en entrée une liste de `Predicate goals` et une liste de `DeclAssertion rules`.
Pour chaque `goal`, on applique la fonction `choose` avec laquelle on met à jour l'`Environnement` et `goals`.

## interprete3.py
Contient la fonction `interprete3` qui prend en entrée l'`AST` d'un programme prolog, puis construit une liste de `DeclAssertion rules` et une liste de `Predicate goals`.
Ensuite, elle utilise la fonction `solve` pour obtenir l'`Environnement` qui prouve les `goals`.

## lancer jalon3.py
Tous les fichiers, à part *jalon3.py*, se trouvent dans le répertoire **jalon3**. Pour exécuter *jalon3.py*, il faut avoir python 3.10.

# Jalon 4 - 28/03/22_10:00

## journal.py 
Contient la classe `Journal` qui permet de stocker des choix sous la forme d'une liste contenant des tuples de trois éléments: une liste de `Predicate goals`, une liste de `DeclAssertion` et une `Environnement env`.
Cette classe contient 3 méthodes:
- addChoix : permet d'ajouter un nouveau choix.
- getChoix : retourne le `goals`, l'`Environnement` et le premier élément de `rules` (et le supprime du `Journal`) du dernier `choix`.
- isEmpty : retourne `True` si le `Journal` est vide, `False` sinon.

## solve.py
Contient la fonction `solve` qui prend en entrée un `Journal j`, une liste de `Predicate goals`, une liste de `DeclAssertion` et un `Environnement env`.
Tant que le `Journal` n'est pas vide, on utilise `getChoix` puis on essaye la fonction `choose`. Si ça réussi, on utilise l'`Environnement` et on ajoute le `body` retourné par `choose` au `goals`. Si ce dernier est vide, on retourne l'`Environnement` sinon on ajoute un nouveau choix au `Journal`.
Si une `exception` est levée, alors on passe à un nouveau choix.

## interprete4.py
Contient la fonction `interprete4` qui prend en entrée l'`AST` d'un programme prolog, puis construit une liste de `DeclAssertion rules` et une liste de `Predicate goals`.
Ensuite, elle utilise la fonction `solve` pour obtenir l'`Environnement` qui prouve les `goals`.
Si une `exception` est levée, alors elle affiche "Faux" sinon, on élimine de l'`Environnement` les subtitutions des variables qui ne sont pas initialement dans les `goals` et s'il ne reste aucune subtitution alors on retourne "Vrai" sinon on retourne l'`Environnement` qui résout le système.

## lancer jalon4.py
Tous les fichiers, à part *jalon4.py*, se trouvent dans le répertoire **jalon4**. Pour exécuter *jalon4.py*, il faut avoir python 3.10.

# Jalon 5 - 04/04/22_10:00

## toplevel.py 
Contient la fonction `toplevel` qui prend en entrée le `path` d'un programme prolog puis demande à l'utilisateur d'écrire une requête puis affiche une solution.

## interprete5.py
Contient la fonction `interprete5` qui prend en entrée le `path` d'un programme prolog puis demande à l'utilisateur d'écrire une requête, puis va utiliser la fonction `solve` du jalon4. L'environnement ainsi obtenu passe par plusieurs filtres qui mettent en forme la solution puis l'affiche.

## trace
En écrivant ?- trace. cela active l'affichage de trace.

## lancer jalon5.py
Tous les fichiers, à part *jalon5.py*, se trouvent dans le répertoire **jalon5**. Pour exécuter *jalon5.py*, il faut avoir python 3.10.